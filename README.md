# Set Timedate Gui

A zenity GUI for timedatectl. 

This script was created by a friend of mine, whose PC's clock is broken. 
So on each reboot, he would need to set the system date and time.

This script can ease the pain. 
It stores the latest known date and time in a file and provides a GUI to manually set it.

I open sourced it with his permission, just in case someone needs it.

## Usage

Run it as follows:

```bash
./SysDateCtl.sh
```

A GUI will ask you for the date and time, which will be set and saved.

## Unit Testing

Test this script using docker.

Initially, you need to build the docker image once:

```bash
$ ./bats-docker -b 
```

Then run the tests:

```bash
$ ./bats-docker
```

See `./bats-docker -h` and [the bats-core documentation](https://github.com/bats-core/bats-core) 
for help. You can also pass through bats parameters.

## Automatic testing and docker images

This project provides a [.gitlab-ci.yml](.gitlab-ci.yml) and thus uses GitLab CI/CD.

Build and publish the images for gitlab CI 
([also see gitlab docs](https://docs.gitlab.com/ee/user/packages/container_registry/index.html)):

```bash
$ docker login registry.gitlab.com
$ docker build \
  --no-cache \
  -t registry.gitlab.com/toolcollection/set-timedate-gui:bats-mock-alpine-latest \
  --file ci/bats-mock-alpine-latest.df \
  ci
$ docker push registry.gitlab.com/toolcollection/set-timedate-gui:bats-mock-alpine-latest
```

## Known Issues

- zenity forms don't allow to prepopulate input fields 
  (would work with [yad](https://wiki.ubuntuusers.de/yad/))
- there's no input validation
