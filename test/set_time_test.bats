#!/usr/bin/env bats

load helpers/sanity_checks
load helpers/recall_date
load helpers/print
load mock/control/zenity
load mock/control/date

setup() {
  sanity_checks
  reset_zenity_return_value
  reset_date
  reset_recall_date
}

CMD="/code/SysDateCtl.sh"

@test "It refreshes the system date if it is older than the recall date" {
  local expected="2020-01-01 00:00:00"
  set_zenity_canceled
  local lost_system_date="1970-01-01 00:00:00"
  set_date "${lost_system_date}"
  set_recall_date "${expected}"

  run_sysdatectl_and_assert_status 0

  [[ "$(get_date_Ymd_HMS)" == "${expected}" ]]
}

run_sysdatectl_and_assert_status() {
  local expected_status=${1}
  run "${CMD}"
  print "${output[@]}"
  [[ "${status}" -eq "${expected_status}" ]]
}
