#!/usr/bin/env bash

reset_recall_date() {
  set_recall_date "${TIME_MOCK_STANDARD_DATE_TIME}"
}

set_recall_date() {
  local time=${1}
  echo "${time}" > ./SysDateSave.txt
}