#!/usr/bin/env bash

print() {
  print_hint "$@"
}

print_hint() {
  (>&3 echo -e "\033[93m${1}\033[0m")
}

print_error() {
  (>&3 echo -e "\033[91m${1}\033[0m")
}