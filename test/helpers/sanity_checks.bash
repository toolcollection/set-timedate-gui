#!/usr/bin/env bash

sanity_checks() {
  verify_required_programs_exist

  verify_zenity_data_dir_is_configured
  verify_zenity_standard_date_time_is_set

  verify_time_data_dir_is_configured
  verify_time_standard_date_time_is_set
}

verify_required_programs_exist() {
  local required_programs=(timedatectl zenity)
  for program in "${required_programs[@]}"
  do
    verify_installed "${program}"
  done
}

verify_installed() {
  local cmd_which="command -v"
  local program=${1}

  $cmd_which "$program" || print_error "Required $program not installed. Please install and rerun."
}

verify_zenity_data_dir_is_configured() {
  # -v checks if the given variable is set and requires bash 4.2
  [[ -v ZENITY_DATA_DIR ]] || exit_with_error "Expected variable ZENITY_DATA_DIR not set. You should 'export ZENITY_DATA_DIR=./your-path' or run this inside testing container."
}

verify_zenity_standard_date_time_is_set() {
  # -v checks if the given variable is set and requires bash 4.2
  [[ -v ZENITY_MOCK_STANDARD_DATE_TIME ]] \
    || exit_with_error "Expected variable ZENITY_MOCK_STANDARD_DATE_TIME not set. You should 'export ZENITY_MOCK_STANDARD_DATE_TIME=\"2020-01-01 14:00\" or run this inside testing container."
}

verify_time_data_dir_is_configured() {
  # -v checks if the given variable is set and requires bash 4.2
  [[ -v TIME_DATA_DIR ]] || exit_with_error "Expected variable TIME_DATA_DIR not set. You should 'export TIME_DATA_DIR=./your-path' or run this inside testing container."
}

verify_time_standard_date_time_is_set() {
  # -v checks if the given variable is set and requires bash 4.2
  [[ -v TIME_MOCK_STANDARD_DATE_TIME ]] \
    || exit_with_error "Expected variable TIME_MOCK_STANDARD_DATE_TIME not set. You should 'export TIME_MOCK_STANDARD_DATE_TIME=\"2020-01-01 14:00\" or run this inside testing container."
}