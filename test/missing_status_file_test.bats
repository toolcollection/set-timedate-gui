#!/usr/bin/env bats

load helpers/sanity_checks
load helpers/print
load mock/control/zenity
load mock/control/date

DATE_RECALL_FILE="SysDateSave.txt"

CMD="/code/SysDateCtl.sh"

setup() {
  sanity_checks
  reset_zenity_return_value
  reset_date
  remove_file_and_verify_its_gone "${DATE_RECALL_FILE}"
}

# shellcheck disable=SC2030
@test "it creates a missing date recall file" {
  run_sysdatectl_and_assert_status 0
  [[ -f "${DATE_RECALL_FILE}" ]]
}

remove_file_and_verify_its_gone() {
  local file=${1}
  [[ -e "${file}" ]] && rm "${file}"
  [[ ! -f "${file}" ]]
}

run_sysdatectl_and_assert_status() {
  local expected_status=${1}
  run ${CMD}
  [[ "${status}" -eq "${expected_status}" ]]
}

@test "it stores the zenity input even if the date recall file was missing before" {
  local expected="2020-02-04 08:03:00"

  set_zenity_return_value "${expected}"
  run_sysdatectl_and_assert_status 0

  [[ "$(get_date_Ymd_HMS)" == "${expected}" ]]
  [[ "$(cat ${DATE_RECALL_FILE})" == "${expected}" ]]
}