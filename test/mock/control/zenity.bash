#!/usr/bin/env bash

reset_zenity_return_value() {
  set_zenity_return_value "${ZENITY_MOCK_STANDARD_DATE_TIME}"
}

set_zenity_return_value() {
  local value="${1}"
  local path
  path="$(get_zenity_date_file_path)"

  echo "${value}" > "${path}"
}

get_zenity_date_file_path() {
  local base_dir="${ZENITY_DATA_DIR}"
  local file_name="zenity-return-value"

  echo "${base_dir}/${file_name}"
}

set_zenity_canceled() {
  local path
  path="$(get_zenity_date_file_path)"
  rm "$path"
}