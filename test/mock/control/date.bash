#!/usr/bin/env bash

get_date_Ymd_HMS() {
  get_date +"%Y-%m-%d %H:%M:%S"
}

get_date() {
  local date_mock_file
  date_mock_file="$(get_date_mock_file_path "mock-system-time")"
  if [[ ! -e $date_mock_file ]]; then
    create_mock_file "mock-system-time"
  fi
  original_date --reference="${date_mock_file}" "$@"
}

original_date() {
  /bin/busybox date "$@"
}

get_date_mock_file_path() {
  local base_dir="${TIME_DATA_DIR}"
  local file_name="${1}"

  echo "${base_dir}/${file_name}"
}

delete_mock_file() {
  local mock_file
  mock_file=$(get_date_mock_file_path "${1}")
  if [[ -f ${mock_file} ]]; then
    rm "${mock_file}"
  fi
}

create_mock_file() {
  local mock_file
  mock_file=$(get_date_mock_file_path "${1}")

  touch "${mock_file}"
}

reset_date() {
  set_date "${TIME_MOCK_STANDARD_DATE_TIME}"
}

set_date() {
  local date="${1:-}"
  set_mock_file_date "mock-system-time" "${date}"
}
set_mock_file_date() {
  local mock_file_name="${1}"
  local mock_file_path
  mock_file_path=$(get_date_mock_file_path "${mock_file_name}")
  local date="${2:-}"

  touch -d "${date}" "${mock_file_path}"
}
