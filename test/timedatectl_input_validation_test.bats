#!/usr/bin/env bats

load helpers/sanity_checks
load helpers/print
load mock/control/zenity
load mock/control/date
load helpers/recall_date

setup() {
  sanity_checks
  reset_zenity_return_value
  reset_date
  reset_recall_date
}

CMD="/code/SysDateCtl.sh"

@test "It doesn't accept 'abcdef'" {
  assert_timedatectl_hasnt_changed "abcdef"
}

assert_timedatectl_hasnt_changed() {
  local input=${1}
  local expected=${TIME_MOCK_STANDARD_DATE_TIME}
  set_zenity_return_value "${input}"
  run_sysdatectl_and_assert_status_failed
  local result
  result="$(get_date_Ymd_HMS)"
  [ "${result}" == "${expected}" ]
}

run_sysdatectl_and_assert_status_failed() {
  run "${CMD}"
  [ "${status}" -eq 1 ]
}

@test "It doesn't accept 'YYYY-mm-dd ' (with trailing space)" {
  # This is what zenity returns if we don't specify a time
  assert_timedatectl_hasnt_changed "2020-02-28 "
}