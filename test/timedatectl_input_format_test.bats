#!/usr/bin/env bats

load helpers/sanity_checks
load helpers/print
load mock/control/zenity
load mock/control/date
load helpers/recall_date

setup() {
  sanity_checks
  reset_zenity_return_value
  reset_date
  reset_recall_date
}

CMD="/code/SysDateCtl.sh"

@test "It accepts 'YYYY-mm-dd' and keeps the current time in that case" {
  assert_result "2021-12-12" "2021-12-12 14:00:00"
}

assert_result() {
  local input=${1}
  local expected=${2}

  set_zenity_return_value "${input}"
  run_sysdatectl_and_assert_status 0
  [ "$(get_date_Ymd_HMS)" == "${expected}" ]
}

run_sysdatectl_and_assert_status() {
  local expected_status=${1}
  run "${CMD}"
  [ "${status}" -eq "${expected_status}" ]
}

@test "It accepts 'YYYY-mm-dd HH:MM'" {
  assert_result "2020-01-01 16:30" "2020-01-01 16:30:00"
}

@test "It accepts 'YYYY-mm-dd HH:MM:SS'" {
  assert_result "2022-12-31 13:40:35" "2022-12-31 13:40:35"
}
