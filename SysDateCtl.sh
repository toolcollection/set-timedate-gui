#!/usr/bin/env bash

# exit when a command fails
set -o errexit
# return the exit status of the last command that threw a non-zero exit code
set -o pipefail
# exit when script tries to use undeclared variables
set -o nounset

main() {
  local date_recall_file="./SysDateSave.txt"

  refresh_system_time "${date_recall_file}"

  adjust_time "${date_recall_file}"
}

refresh_system_time() {
  local recall_file=${1} file_date
  file_date=$(recall_time "${recall_file}")

  update_system_time_if_older_than "${file_date}"
}

recall_time() {
  create_if_missing "${date_recall_file}"
  cat "${date_recall_file}"
}

create_if_missing() {
  local file=${1}
  if [[ ! -f ${file} ]]; then
    touch "${file}"
  fi
}

update_system_time_if_older_than() {
  local other_date=${1} date_formatted sys_date_formatted
  date_formatted=$(format_date "${other_date}")
  sys_date_formatted=$(format_date)

  if [[ $sys_date_formatted < $date_formatted ]]; then
    set_time "${date_formatted}"
  fi
}

format_date() {
  local other_date=${1:-}
  local format_pattern="${2:-"%Y-%m-%d %T"}"
  if [[ -n ${other_date} ]]; then
    date --date="${other_date}" +"${format_pattern}"
  else
    date +"${format_pattern}"
  fi
}

set_time() {
  local date=${1}
  timedatectl set-ntp 0
  timedatectl set-time "${date}"
  timedatectl set-ntp 1
}

adjust_time() {
  local date_recall_file=${1}
  local adjusted_datetime
  if adjusted_datetime=$(get_adjusted_datetime); then
      local date_with_time
      date_with_time=$(fill_in_current_time_if_missing "${adjusted_datetime}")
      set_store_and_output_time "${date_with_time}" "${date_recall_file}"
  fi
}

get_adjusted_datetime() {
  zenity \
    --forms \
    --add-calendar= \
    --forms-date-format="%Y-%m-%d" \
    --add-entry="Uhrzeit (13:00)" \
    --text="Systemdatum eingeben" \
    --separator=" "
}

fill_in_current_time_if_missing() {
  local given_datetime=${1}
  if [[ $given_datetime =~ ^[0-9-]+[[:blank:]][0-9\:]+$ ]]; then
    echo "${given_datetime}"
  else
    local date current_time
    date=$(date --date="${given_datetime}" +"%Y-%m-%d")
    current_time=$(date +"%H:%M:%S")
    echo "${date} ${current_time}"
  fi
}

set_store_and_output_time() {
  local adjusted_datetime=${1}
  local date_recall_file=${2}

  set_time "${adjusted_datetime}"

  display_and_store "${adjusted_datetime}"
}

display_and_store() {
  echo "${adjusted_datetime}" | tee "${date_recall_file}"
}

main "$@"
